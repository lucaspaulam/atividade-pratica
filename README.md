# Questões práticas

## 1. A essa altura, você já deve ter criado a sua conta do GitLab, não é? Crie um repositório público na sua conta, que vai se chamar Atividade Prática e por fim sincronize esse repositório em sua máquina local.
R: Repositório criado
## 2. Dentro do seu reposotorio, crie um arquivo chamado README.md e leia o artigo como fazer um readme.md bonitão e deixe esse README.md abaixo bem bonitão:
## README.md onde o trainne irá continuamente responder as perguntas em formas de commit.
R: README.md criado
## 3. Crie nesse repositório um arquivo que vai se chamar calculadora.js, abra esse arquivo em seu editor de códigos favoritos e adicione o seguinte código:
## node calculadora.js a b
```
const args = process.argv.slice(2);
console.log(parseInt(args[0]) + parseInt(args[1]));
```
## Descubra o que esse código faz através de pesquisas na internet, também descubra como executar um código em javascript e dado que o nome do nosso arquivo é calculadora.js e você entendeu o que o código faz, escreva abaixo como executar esse código em seu terminal:

R: A primeira linha do código armazena na constante args os dados que são fornecidos na linha de comando, excluindo o caminho do nó e o caminho do código, pois o comando “.slice(2)” após o “process.argv” garante isso. Na segunda linha é mandado para o console o retorno das funções “parseInt(args[0])” + “parseInt(args[1])” , que irão converter a string contida em “args[0]” e “args[1]” em inteiros e retornar a soma deles.
Após a instalação do Node.js podemos executar arquivos .js pelo comando “node (nome do arquivo.exensão”. Rodando a linha de comando “node calculadora.js a b” o script nos retorna a mensagem NaN, o que significa  que Not a Number, pois passamos caracteres que representam letras e a função parseInt retorna NanN para este tipo de caractere. Se mandarmos um “node calculadora.js 1 2”, nos retornará a soma destes números.
## 4. Agora que você já tem um código feito e a resposta aqui, você precisa subir isso para seu repositório. Sem usar git add . descubra como adicionar apenas um arquivo ao seu histórico de commit e adicione calculadora.js a ele.

## Que tipo de commit esse código deve ter de acordo ao conventional commit?
## Que tipo de commit o seu README.md deve contar de acordo ao conventional commit. Por fim, faça um push desse commit?

R: Para adicionar um arquivo específico ao histórico de commit podemos utilizar o git add -i, que abre uma tela interativa do terminal. Após isso , digitamos 1 para ver o status atual do staging area, e clicando em 2 temos a lista de arquivos que podem ser adicionados nela.Agora você escolhe o número correspondente ao arquivo que quer adicionar ao staging, e após isso ele será mostrado com um asterístico na frente do nome do arquivo. Após isso é só clicar em enter que o arquivo será adicionado ao staging área.
Para o caso de adição de funcionalidade usamos o commit “feat
Já para a modificação de docs, como o README, utilizamos o commin “docs: “.

## 5. Copie e cole o código abaixo em sua calculadora.js:
```
const soma = () => {
    console.log(parseInt(args[0]) + parseInt(args[1]));
};

const args = process.argv.slice(2);

soma();
```
## Descubra o que essa mudança representa em relação ao conventional commit e faça o devido commit dessa mudança.

R: Usamos o commit "refactor: " pois houveram mudanças no código, mas as funcionalidaes continuam as mesmas.

## 6. João entrou em seu repositório e o deixou da seguinte maneira:
```
const soma = () => {
    console.log(parseInt(args[0]) + parseInt(args[1]));
};

const sub = () => {
    console.log(parseInt(args[0]) - parseInt(args[1]));  
}

const args = process.argv.slice(2);

switch (args[0]) {
    case 'soma':
        soma();
    break;

    case 'sub':
        sub();
    break;

    default:
        console.log('does not support', arg[0]);
}

```
R: Depois disso, realizou um git add . 
e um commit com a mensagem: "Feature: added subtraction"
faça como ele e descubra como executar o seu novo código.

Nesse código, temos um pequeno erro, encontre-o e corrija 
para que a soma e divisão funcionem.

Por fim, commit sua mudança.

Modificando o código para:
```
const soma = () => {
    console.log(parseInt(args[1]) + parseInt(args[2]));
};

const sub = () => {
    console.log(parseInt(args[1]) - parseInt(args[2]));  
}
const args = process.argv.slice(2);

switch (args[0]) {
    case 'soma':
        soma();
    break;

    case 'sub':
        sub();
    break;

    default:
        console.log('does not support', arg[0]);
}
```
Para executar o código usamos o comando “node calculadora.js função a b”, onde função deve assumir soma ou sub, de acordo com a operação desejada.

## 7. Por causa de joãozinho, você foi obrigado a fazer correções na sua branch principal! O produto foi pro saco e a empresa perdeu muito dinheiro porque não conseguiu fazer as suas contas, graças a isso o seu chefe ficou bem bravo e mandou você dar um jeito disso nunca acontecer.

## Aprenda a criar uma branch, e desenvolva a feature de divisão nessa branch.

R: Após criar as branches develop e staging pelos comandos "git checkout -b develop master"
e "git checkout -b staging develop", podemos subir as alterações para a branch desejada com o comando "git push -u origin nome_da_branch"

## 8. Agora que a sua divisão está funcionando e você garantiu que não afetou as outras funções, você está apto a fazer um merge request Em seu gitlab, descubra como realizá-lo de acordo com o gitflow.

R: Para fazer o merge request, iremos na issue referente a modificação no sprint backlog, arrasta-la para doing, clicamos com o botão direito sobre a issue em Create a merge request, e em source escrevemos o nome da branch que iremos utilizar para realizar a feature branch, que no Gitflow seria a develop, e clicar em Create merge develop.
Após isso, vamos à página active branches e pgamos o nome que foi dado ao ramo da issue. Agora em nosso repositório local usamos o código  “git fetch origin nome da branch”  que busca a branch criada no repositório remoto e depois “git checkout nome da branch” que cria uma branch local que será depois linkada à branch que criamos no repositório remoto.
Após a implementação, fazemos o commit e o git pull das modificações para o repositório remoto.
Após implementado e carregado, vamos ao board da sprint e arrastamos ela para “waiting acceptance”, e após a liderança aceitar a issue resolvida, podemos ir em “Mark as ready” e em “merge” para a branch desejada, seja ela direto para a develop, ou no caso do Gitflow, passará para o Staging para realização dos testes para então ser implementado na branch Master.

## 9. João quis se redimir dos pecados e fez uma melhoria em seu código, mas ainda assim, continuou fazendo um push na master, faça a seguinte alteração no código e fez o commit com a mensagem:
## "refactor: calculator abstraction"
```
var x = args[0];
var y = args[2];
var operator = args[1];

function evaluate(param1, param2, operator) {
  return eval(param1 + operator + param2);
}

if ( console.log( evaluate(x, y, operator) ) ) {}
```
## Para piorar a situação, joão não te contou como executar esse novo código, enquanto você não descobre como executá-lo lendo o código, e seu chefe não descobriu que tudo está comprometido, faça um revert através do seu gitlab para que o produto volte ao normal o quanto antes!

R: Resolvido utilizando o comando "git revert HEAD~1" e depois um "git push -u origin master"

## 10. Descubra como executar esse novo código e que operações ele é capaz de realizar. Deixe sua resposta aqui, e explique o que essas funções javascript fazem.

R: A função "eval" nos interpreta os símbolos dos operadores, fazendo com que nosso arquivo possa conter entradas do tipo "1 + 2" , interpretando o conteúdo central como operador da calculadora, fazendo com que nossa calculadora possa fazer todas as operações básicas.

